#!/bin/bash
set -e
clear
echo "remove old db"
rm db.dat

echo "create ex17 db"
./ex17 db.dat c 5 20 

./ex17 db.dat s 1 ben ben@ben.ben
./ex17 db.dat s 2 lol lol@lol.lol
./ex17 db.dat s 3 lul lul@lul.lul
./ex17 db.dat s 4 lal lal@lal.lal
./ex17 db.dat s 5 lorl lorl@lorl.lol

echo "list"
./ex17 db.dat l
echo "find lol"
./ex17 db.dat f lol
