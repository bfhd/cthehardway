CFLAGS=-Wall -g

all: ex1 ex3 ex4 ex5 ex6 ex8 ex10 ex14 ex15 ex16 ex17 ex18

clean:
	rm -f ex1
	rm -f ex3
	rm -f ex4
	rm -f ex5
	rm -f ex6
	rm -f ex8
	rm -f ex10
	rm -f ex14
	rm -f ex15
	rm -f ex16
	rm -f ex17
	rm -f ex18
