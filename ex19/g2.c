#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <assert.h>
#include "object.h"
#include "ngame.h"
#include "g2.h"

Object MonsterProto = {
	.init = Monster_init,
	.attack = Monster_attack
};

Object RoomProto = {
	.move = Room_move,
	.attack = Room_attack
}
;

int Map_init(void *self) {
	Map *map = self;
	assert(map != NULL);
	//make some rooms for a small map
	Room *one = NEW(Room, "Room 1");
	Room *two = NEW(Room, "Room 2");
	Room *three = NEW(Room, "Room 3");
	Room *four = NEW(Room, "Room 4");
	Room *five = NEW(Room, "Room 5.  There is a maths monster here!");
	//put the bad guy in the arena

	five->bad_guy = NEW(Monster, "The maths monster");
	//setup the map rooms
	one->north = two;
	two->south = one;
	two->north = three;
	three->south = two;
	three->north = four;
	four->south = three;
	four->north = five;
	five->south = four;
	five->north = one;

	//start
	map->start = one;
	map->location = one;
	return 1;
}

Object MapProto = {
	.init = Map_init,
	.move = Map_move,
	.attack = Map_attack
};


int main(int argc, char *argv[]) {
	//simple way to setup the  RNG
	srand(time(NULL));

	//make the map
	Map *game = NEW(Map, "MATHS");

	printf("You enter the ");
	game->location->_(describe)(game->location);

	while (process_input(game)) {
	}
	printf("IT'S OVER\n");
	return 0;
}
