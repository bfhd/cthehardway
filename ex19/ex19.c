#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <assert.h>
#include "object.h"
#include "ngame.h"
#include "ex19.h"

Object MonsterProto = {
	.init = Monster_init,
	.attack = Monster_attack
};

Object RoomProto = {
	.move = Room_move,
	.attack = Room_attack
}
;
int Map_init(void *self) {
	Map *map = self;
	assert(map != NULL);
	//make some rooms for a small map
	Room *hall = NEW(Room, "The great Hall");
	Room *throne = NEW(Room, "The throne room");
	Room *arena = NEW(Room, "The arena, with the minotaur!");
	Room *kitchen = NEW(Room, "Kitchen - where you find a knife!");
	Room *toilet = NEW(Room, "Toilet - where the poos are");
	Room *bedroom = NEW(Room, "Bedroom - you take a nap and wake up refreshed!");
	Room *closet = NEW(Room, "Closet - there's a rat in here!");
	//put the bad guy in the arena

	arena->bad_guy = NEW(Monster, "The evil minotaur");
	closet->bad_guy = NEW(Monster, "A huge rat");
	//setup the map rooms
	hall->north = throne;
	hall->east = toilet;
	hall->west = bedroom;

	bedroom->south = closet;	
	bedroom->east = hall;
	
	closet->north = bedroom;
	toilet->west = hall;

	throne->west = arena;
	throne->east = kitchen;
	throne->south = hall;

	arena->east = throne;
	kitchen->west = throne;

	//start the map and the characer off in the hall
	map->start = hall;
	map->location = hall;

	return 1;
}

Object MapProto = {
	.init = Map_init,
	.move = Map_move,
	.attack = Map_attack
};


int main(int argc, char *argv[]) {
	//simple way to setup the  RNG
	srand(time(NULL));

	//make the map
	Map *game = NEW(Map, "The Hall of the Minotaur.");

	printf("You enter the ");
	game->location->_(describe)(game->location);

	while (process_input(game)) {
	}

	return 0;
}
