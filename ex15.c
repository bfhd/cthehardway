#include <stdio.h>

void print_arrays(int *aages, char *anames[]);


int main(int argc, char *argv[]) {
	int ages[] = {23, 43, 12, 89, 2};
	char *names[] = {
		"Alan", "Frank",
		"Mary", "John", "Lisa"
	};
	print_arrays(ages, names);

	return 0;
}

void print_arrays(int *aages, char *anames[]) {
	int count = 5; //= sizeof(ages) / sizeof(int);
	int i = 0;
	printf("%d count\n",count);
	for(i = 0; i < count; i++) {
		printf("%s has %d years alive.\n",anames[i], aages[i]);
	}

	printf("---\n");

	int *cur_age = aages;
	char **cur_name = anames;

	for(i = 0; i < count; i++) {
		printf("%s is %d years old.\n", *(cur_name+i), *(cur_age+i));
	}
	printf("---\n");
    for(i = 0; i < count; i++) {
	    printf("%s is %d years old again.\n", cur_name[i], cur_age[i]);
    }
    printf("---\n");

	for(cur_name = anames, cur_age = aages; (cur_age - aages) < count; cur_name++, cur_age++) {
		printf("%s lived %d years do far.\n",*cur_name, *cur_age);
	}
}
