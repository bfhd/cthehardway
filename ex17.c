#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#define MAX_DATA 512
#define MAX_ROWS 100

struct Address {
	int id;
	int set;
	char *name;
	char *email;
};

struct Database {
	int maxrows;
	int maxdata;
	struct Address **rows;
};

struct Connection {
	FILE *file;
	struct Database *db;
};

void Database_close(struct Connection *conn);

void die(const char * message, struct Connection *conn) {
	if(errno) {
		perror(message);
	} else {
		printf("ERROR: %s\n", message);
	}
	Database_close(conn);
	exit(1);
}

void Address_print(struct Address *addr) {
	printf("%d %s %s\n", addr->id+1, addr->name, addr->email);
}

void Database_load(struct Connection *conn) {
	int rc = fread(&conn->db->maxrows, sizeof(conn->db->maxrows), 1, conn->file); 
	if (rc != 1) die("Failed to read row detail!",conn);
	rc = fread(&conn->db->maxdata, sizeof(conn->db->maxdata), 1, conn->file);
	if (rc != 1) die("Failed to read data detail!",conn);
	
//	printf("%d maxrows\n",conn->db->maxrows);
//	printf("%d maxdata\n",conn->db->maxdata);
//todo for each item - allocate memory, read it into that memory from disk
	//rc = fread(conn->db, sizeof(struct Database), 1, conn->file); //why load database from file
	//if (rc != 1) die("Failed to load databse.");
	conn->db->rows = malloc(sizeof(struct Address*) * conn->db->maxrows);
	if (!(conn->db->rows)) die("Memory error1 - Address",conn);
	//do we need to malloc pointers? - yes
//	rc = fread(conn->db->rows, sizeof(struct Address *) * conn->db->maxrows, 1, conn->file);
//	if (rc != 1) die("Failed to load Addrss data!");
//why load this?
	int i = 0;
	for (i = 0; i < conn->db->maxrows; i++) {
		conn->db->rows[i] = malloc(sizeof(struct Address));
		struct Address *row = conn->db->rows[i];
		rc = fread(&row->id, sizeof(row->id), 1, conn->file);
		if (rc != 1) die("Failed to load address id!",conn);
		rc = fread(&row->set, sizeof(row->set), 1, conn->file);
		//printf("row: %d, set: %d\n",row->id,row->set);
		if (rc != 1) die("Failed to load set value!",conn);
		row->name = malloc(sizeof(*row->name) * conn->db->maxdata);
		row->email = malloc(sizeof(*row->email) * conn->db->maxdata);
		rc = fread(row->name, conn->db->maxdata * sizeof(*row->name), 1, conn->file);
		if (rc != 1) die("Failed to load name!",conn);
		rc = fread(row->email, conn->db->maxdata * sizeof(*row->email), 1, conn->file);
		if (rc != 1) die("Failed to load email!",conn);
		
	}
}

struct Connection *Database_open(const char *filename, char mode, int id) {
	struct Connection *conn = malloc(sizeof(struct Connection));
	conn->db = malloc(sizeof(struct Database));// + (sizeof(int)*2));
	if(!conn->db) die("Memory error",conn);

	if(mode == 'c') {
		conn->file = fopen(filename, "w");
	} else {
		conn->file = fopen(filename,"r+");
		if(conn->file) {
			Database_load(conn);
		}
		if(id > conn->db->maxrows) die("There's not that many records!",conn); 
	}
	if(!conn->file) die("Failed to open the file",conn);

	return conn;
}

void Database_close(struct Connection *conn) {
	if(conn) {
		if(conn->file) fclose(conn->file);
		if(conn->db) {
			int i;
			for(i = 0; i < conn->db->maxrows; i++) {
				free(conn->db->rows[i]->name);
				free(conn->db->rows[i]->email);
				free(conn->db->rows[i]);
			}
			free(conn->db->rows);
			free(conn->db);
		free(conn);
		}
	}
}

void Database_write(struct Connection *conn) {
	rewind(conn->file);
	int lc = fwrite(&conn->db->maxrows, sizeof(conn->db->maxrows), 1, conn->file);
	if (lc != 1) die("Failed to write maxrows to database",conn);
	lc = fwrite(&conn->db->maxdata, sizeof(conn->db->maxdata), 1, conn->file);
	if (lc != 1) die("Failed to write maxdata database",conn);
//todo
	//int rc = fwrite(conn->db, sizeof(struct Database *), 1, conn->file); why write this to disk
//	if(rc != 1) die("Failed to write database.");
	//int rc = fwrite(conn->db->rows, sizeof(struct Address *)  * conn->db->maxrows,1 , conn->file);
	int i, rc;
	for (i = 0;i < conn->db->maxrows; i++) { 
		rc = fwrite(&conn->db->rows[i]->id, sizeof(conn->db->rows[i]->id), 1, conn->file);
		//printf("set = %d, i = %d, name = %s, email = %s",conn->db->rows[i]->set,i,conn->db->rows[i]->name,conn->db->rows[i]->email);
		if(rc != 1) die("Disk write error: id info!",conn);
		rc = fwrite(&conn->db->rows[i]->set, sizeof(conn->db->rows[i]->set), 1, conn->file);
		if(rc != 1) die("Disk write error: set info!",conn);
		rc = fwrite(conn->db->rows[i]->name, sizeof(char) * conn->db->maxdata, 1, conn->file);
		if(rc != 1) die("Disk write error: name info!",conn);
		rc = fwrite(conn->db->rows[i]->email,  sizeof(char) * conn->db->maxdata,1, conn->file);
		if(rc != 1) die("Disk write error: email info!",conn);
	}
	rc = fflush(conn->file);
	if(rc == -1) die("Cannot flush database",conn);


}

void Database_create(struct Connection *conn, int rows, int data) {
	int i = 0;
	conn->db->rows = malloc(sizeof(conn->db->rows) * rows);
	for(i = 0; i < rows; i++) {
		//make prototype to initialise
		//struct Address *addr = malloc(sizeof(struct Address));
		//then assign
		//todo
		conn->db->rows[i] = malloc(sizeof(struct Address));//addr;
		conn->db->rows[i]->id = i;
		conn->db->rows[i]->set = 0;
		conn->db->rows[i]->name = malloc(data);
		memset(conn->db->rows[i]->name,' ',data);
		conn->db->rows[i]->email = malloc(data);
		memset(conn->db->rows[i]->email,' ',data);
	}
	conn->db->maxrows = rows;
	conn->db->maxdata = data;

}

void Database_set(struct Connection *conn, int id, const char *name, const char *email) {
	struct Address *addr = conn->db->rows[id];
	if(addr->set) die("Already set, delete it first",conn);

	addr->set = 1;
	//warning, bug, nope
	//re-bugged - segfaults here
	addr->name = malloc(sizeof(char) * conn->db->maxdata);
	addr->email = malloc(sizeof(char) * conn->db->maxdata);

	char *res = strncpy(addr->name, name, conn->db->maxdata);
	addr->name[conn->db->maxdata-1] = '\0';
	//demonstrate big
	if(!res) die("Name copy failed",conn);
	res = strncpy(addr->email, email, conn->db->maxdata);
	addr->email[conn->db->maxdata] = '\0';
	if(!res) die("Email copy failed",conn);

}

void Database_get(struct Connection *conn, int id) {
	struct Address *addr = conn->db->rows[id];

	if(addr->set) {
		Address_print(addr);
	} else {
		die("ID is not set",conn);
	}
}

void Database_delete(struct Connection *conn, int id) {
	struct Address addr = {.id = id, .set = 0};
	conn->db->rows[id] = &addr;
}

void Database_list(struct Connection *conn) {
	int i = 0;
	struct Database *db = conn->db;
	
	for(i = 0; i < db->maxrows; i++) {
		struct Address *cur = db->rows[i];

		if(cur->set) {
			Address_print(cur);
		}
	}
	//printf("%d rows, %d data\n", conn->db->maxrows, conn->db->maxdata);
}

void Database_find(struct Connection *conn, char *sstring) {
//	printf("Not implemented.  Here's what you searched for: %s\n",sstring);
	int i = 0;
	for (i = 0; i < conn->db->maxrows; i++) {
		struct Address *cur = conn->db->rows[i];
		if (cur->set == 1 && strstr(cur->name, sstring) != NULL) {
			Address_print(cur);
		} else if (cur->set == 1 && strstr(cur->email, sstring) != NULL) {
			Address_print(cur);
		}
	}
}

int main(int argc, char *argv[]) {
	if(argc < 3) die("USAGE: ex17 <dbfile> <action> [action params]",NULL);

	char *filename = argv[1];
	char action = argv[2][0];
	int id = 0;
	if(argc > 3) id = atoi(argv[3]);
	struct Connection *conn = Database_open(filename, action, id);

	switch(action) {
		case 'c':
			if (argc != 5) die("Need to specify max rows and data!",conn);
			Database_create(conn, atoi(argv[3]), atoi(argv[4])); //3 = rows, 4 = data
			Database_write(conn);
			break;
		
		case 'g':
			if (argc != 4) die("Need an id to get",conn);
			Database_get(conn, id-1);
			break;

		case 's':
			if (argc != 6) die("Need id, name, email to set",conn);

			Database_set(conn, id-1, argv[4], argv[5]);
			Database_write(conn);
			break;

		case 'd':
			if (argc != 4) die("Need id to delete",conn);

			Database_delete(conn, id-1);
			Database_write(conn);
			break;

		case 'l':
			Database_list(conn);
			break;
		
		case 'f':
			if (argc != 4) die("Need search string!",conn);
			Database_find(conn,argv[3]);
			break;
			
		default:
			die("Invalid action, only: c=create, g=get, s=set, d=delete, l=list, f=find",conn);
	}

	Database_close(conn);

	return 0;
}


/*
What did I learn?
writing files to disk - make sure you get the number of bytes correct with fwrite
make sure your reads line up with your writes
make sure the size of read/writes/mallocs is the same


*/

